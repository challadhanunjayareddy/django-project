from django.apps import AppConfig


class RailwayticketConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'railwayticket'
