from django.db import models

# Create your models here.

class Station(models.Model):
    name=models.CharField(max_length=500)

class Train(models.Model):
    name=models.CharField(max_length=500)
    stationname=models.CharField(max_length=500)

class Seat_Chart(models.Model):
    train = models.ForeignKey(Train, on_delete=models.CASCADE, related_name="train_chart")
    first_ac = models.IntegerField("1st AC")
    second_ac = models.IntegerField("2nd AC")
    third_ac = models.IntegerField("3rd AC")
    sleeper = models.IntegerField("Sleeper")
